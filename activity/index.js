const http = require(`http`);

const port = 3000;
const server = http.createServer((request, response) => {
	if(request.url == `/login`){
		response.writeHead(200, {'Content-Type': 'text/plain'});
		response.end(`You are in the loginpage`);
	}else{
		response.writeHead(404, {'Content-Type': 'text/plain'});
		response.end(`Page not found`);
	}

})

server.listen(port);

console.log(`Server now accessible at localhost: ${port}.`);